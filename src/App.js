import "./style/App.css";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import positions from "./fabriques.json";
import { useState } from "react";
import Header from "./composants/header";
/* import { createPortal } from "react-dom"; */
import Footer from "./composants/footer";

/* var iconPerson = new L.Icon({
  iconUrl:
      "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png",

  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41],
});
var L = window.L; */
function App() {
  const [data, setdata] = useState(null);

  const [i, seti] = useState([0, 0]);

  navigator.geolocation.getCurrentPosition(function (position) {
    let a = position.coords.latitude;
    let b = position.coords.longitude;

    seti([a, b]);
  });

  const [temps, settemps] = useState(null);

  function Meteo(lat, lon) {
    let latText = "lat=" + lat + "&";
    let longText = "lon=" + lon + "&";
    let clé = "appid=807b4849d8830c65f707385edcb8e2fe&units=metric";
    let lien =
      "https://api.openweathermap.org/data/2.5/weather?" +
      latText +
      longText +
      clé;

    fetch(lien)
      .then(function (reponse) {
        return reponse.json();
      })
      .then(function (monjson) {
        settemps(monjson);
      })
      .catch((e) => console.log(e));
  }

  return (
    <div className="container">
      <Header />
      <p className="centre">Simplon près de chez vous</p>
      {/*  AFFICHER LA CARTE ET LES MARQUEURS */}
      <div className="infosmap">
        <div className="mapid">
          <MapContainer center={[43.6487851, 2.3435684]} zoom={7}>
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {positions.map((ville) => (
              <Marker /* icon={iconPerson} */
                position={[ville.latitude, ville.longitude]}
                key={ville.nom}
                eventHandlers={{
                  click: () => {
                    setdata(ville);
                    Meteo(ville.latitude, ville.longitude);
                  },
                }}
              >
                {" "}
                <Popup className="popup">
                  <p> {[ville.nom]}, </p>
                  <p>{[ville.formation]}</p>
                  <p>{temps?.weather[0]?.main}</p>
                  <p>{temps?.main.temp}</p>
                  <p>{temps?.main.humidity}</p>
                </Popup>
              </Marker>
            ))}

            <Marker
              position={i}
              eventHandlers={{
                click: () => {
                  Meteo(i[0], i[1]);
                },
              }}
            >
              {" "}
              <Popup>
                <p>{temps?.name}</p>
                <p>{temps?.weather[0]?.main}</p>
                <p>{temps?.main.temp}</p>
                <p>{temps?.main.humidity}</p>
              </Popup>
            </Marker>
          </MapContainer>
        </div>
        {/* AFFICHER LES INFOS DANS DIV AU CLICK DES MARQUEURS */}
        <div className="madivinfos">
          {data ? (
            <ul>
              <li className="nom">{data.nom}</li>
              <li className="monadresse">{data.adresse}</li>
              <li className="formation">{data.formation}</li>
            </ul>
          ) : (
            ""
          )}
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
