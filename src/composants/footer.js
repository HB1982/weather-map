import logofooter from "../images/logo-footer.svg";

function Footer() {
  return (
    <footer>
      <img className="logofooter" src={logofooter} alt="Logofooter" />

      <div className="monfooter">
        Mentions légales | Nos actualités| Nous rejoindre | Contact | Presse|
        Plan du site | Notre agence numérique
      </div>
    </footer>
  );
}
export default Footer;
