import logo from "../images/téléchargementl.png";
import facebooklogo from "../images/facebook.svg";
import instalogo from "../images/instagram.svg";
import linkedin from "../images/linkedin.svg";
import photo from "../images/Header-Simplon-Occitanie.jpg";
function Header() {
  return (
    <header>
      <div className="navheader">
        <div className="haut">
          <div className="simplonheader">
            <i className="fa-solid fa-universal-access"></i>
            <p className="p">SIMPLON.CO</p>
          </div>
          <div className="mesliens">
            <img className="fblogo" src={facebooklogo} alt="fbLogo" />
            <img className="instalogo" src={instalogo} alt="instagramLogo" />
            <img className="lklogo" src={linkedin} alt="linkedinLogo" />
          </div>
        </div>

        <div className="bas">
          <div className="logo">
            <img className="logo" src={logo} alt="Logo" />
          </div>
          <div className="Menu">
            <ul>
              <li>VOUS VOULEZ</li>
              <li>WELCODE</li>
              <li>NOS ACTUALITES</li>
            </ul>
          </div>
        </div>
      </div>

      <div className="textheader">
        <div className="texte">
          <h2>Simplon.co en Occitanie</h2>
          <p>
            Simplon.co est un réseau de Fabriques solidaires et inclusives qui
            proposent des formations gratuites aux métiers techniques du
            numérique en France et à l’étranger
          </p>
          <div className="bouton">
            <p>FORMATIONS OUVERTES</p>
          </div>
        </div>
        <div>
          <img className="photo" src={photo} alt="photo" />
        </div>
      </div>
    </header>
  );
}

export default Header;
