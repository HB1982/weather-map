import { useState } from "react";
import "../fabriques.json";

function MesInfos(props) {
  const [data, setdata] = useState(null);

  return (
    <div className="madivinfos">
      {data ? (
        <ul>
          <li>{data.nom}</li>
          <li>{data.formation}</li>
        </ul>
      ) : (
        ""
      )}
    </div>
  );
}

export default MesInfos;
